defmodule Chefskitchen.GuardianSerializer do
    @behaviour Guardian.Serializer
    
    def for_token(%Chefskitchen.User{} = user), do: {:ok, "User:#{user.email}"}
    def for_token(_), do: {:error, "Unknown Resource"}

    def from_token("User:"<>user_email), do: {:ok, Chefskitchen.Repo.get_by!(Chefskitchen.User, email: user_email)}
    def from_token(_), do: {:error, "Unknown Resource"}
  end