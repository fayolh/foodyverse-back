defmodule Chefskitchen.DiningTableChat do
  use Chefskitchen.Web, :model

  schema "dining_table_chats" do
    field :content, :string
    belongs_to :user, Chefskitchen.User, foreign_key: :user_id
    belongs_to :dining_table, Chefskitchen.DiningTable, foreign_key: :dining_table_id

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:content])
    |> validate_required([:content])
  end
end
