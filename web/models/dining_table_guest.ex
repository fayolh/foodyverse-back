defmodule Chefskitchen.DiningTableGuest do
  use Chefskitchen.Web, :model

  schema "dining_table_guests" do
    field :status, :string, default: "PENDING"
    belongs_to :guest, Chefskitchen.User, foreign_key: :user_id
    belongs_to :joined_dining_table, Chefskitchen.DiningTable, foreign_key: :dining_table_id

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:status, :user_id, :dining_table_id])
    |> unique_constraint(:user_id, name: :dining_table_guests_user_id_dining_table_id_index, message: "user has already requested to join this event")
    |> validate_required([:status])
  end
end
