defmodule Chefskitchen.DiningTable do
  use Chefskitchen.Web, :model
  use Arc.Ecto.Schema

  schema "dining_tables" do
    field :title, :string
    field :description, :string
    field :number_of_guests, :integer
    field :status, :string, default: "OPEN"
    field :location, :string
    field :fee, :float, default: 0
    field :payment_type, :string
    field :time, :naive_datetime
    field :ingredients, {:array, :string}
    field :image, Chefskitchen.Image.Type
    belongs_to :host, Chefskitchen.User, foreign_key: :user_id
    has_many :received_comments, Chefskitchen.DiningTableComment
    has_many :guests, Chefskitchen.DiningTableGuest

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:title, :description, :number_of_guests, :status, :location, :fee, :time, :ingredients, :payment_type])
    |> cast_attachments(params, [:image])
    |> validate_required([:title, :description, :number_of_guests, :status, :location, :fee, :time, :payment_type])
  end

  def image_url(changeset), do: Chefskitchen.Image.url({changeset.image, changeset})
end
