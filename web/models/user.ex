defmodule Chefskitchen.User do
  use Chefskitchen.Web, :model
  use Arc.Ecto.Schema

  schema "users" do
    field :last_name, :string
    field :first_name, :string
    field :gender, :string
    field :email, :string
    field :phone, :string
    field :password, :string, virtual: true
    field :address, :string
    field :about, :string
    field :roles, {:array, :string}, default: ["HOST"]
    field :password_confirmation, :string, virtual: true
    field :languages, {:array, :string}
    field :verified, :boolean, default: false
    field :encrypted_password, :string
    field :status, :string, default: "INACTIVE"
    field :profile_picture, Chefskitchen.Avatar.Type
    
    # has_many :languages, Chefskitchen.Language
    has_many :hosted_dining_tables, Chefskitchen.DiningTable
    has_many :joined_dining_tables, Chefskitchen.DiningTableGuest
    has_many :posted_comments, Chefskitchen.DiningTableComment

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do 
    struct
    |> cast(params, [:last_name, :first_name,:gender, :email, :phone, :password, :password_confirmation, 
                    :languages, :address, :about, :roles, :verified])
    |> cast_attachments(params, [:profile_picture])
    |> validate_required([:last_name, :first_name, :email, :password, :password_confirmation, :roles, :verified])
    |> validate_format(:email, ~r/@/)
    |> unique_constraint(:email)
    |> validate_confirmation(:password)
    |> encrypt_password
  end

  def activation_changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:email, :roles, :verified, :status])
    |> validate_required([:email, :roles, :verified, :status])
  end

  def encrypt_password(changeset) do
    if changeset.valid? do
      put_change(changeset, :encrypted_password, Comeonin.Pbkdf2.hashpwsalt(changeset.changes[:password]))
    else
      changeset
    end
  end
  
  def update_changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:last_name, :first_name,:gender, :email, :phone, :password, :languages, :address, :about, :roles])
    |> cast_attachments(params, [:profile_picture])
    |> validate_format(:email, ~r/@/)
    |> unique_constraint(:email)
  end

  def image_url(changeset), do: Chefskitchen.Avatar.url({changeset.profile_picture, changeset})
  # def image_url(changeset), do: Chefskitchen.Avatar.url({changeset.profile_picture, changeset}, :thumb)
  
end
