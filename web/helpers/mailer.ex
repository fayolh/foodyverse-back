defmodule Chefskitchen.Mailer do
    @config domain: Application.get_env(:chefskitchen, :mailgun_domain),
            key: Application.get_env(:chefskitchen, :mailgun_key)
            # mode: :test,
            # test_file_path: "mailgun.json"
    
    use Mailgun.Client, @config
                        
  
    @from "info@foodyverse.ee"

    def send_activation_email(user, url) do
        send_email to: user.email,
                    from: @from,
                    subject: "FoodyVerse account activation",
                    # attachments: [%{path: "salemove.pdf" , filename: "sale.pdf"}],
                    html: " <strong>Hello!</strong>
                    <p>You have recently registered for an account at FoodyVerse.</p>
                    <p>To activate your account, click the link below.</p>
                    
                   <div>
                       <a href=\"#{url}\"> Activate your account </a> 
                   </div>
                    
                      
                   <p>If you did not register for a FoodyVerse account, you can safely ignore this email.</p>
                   <p>If you have any questions just reply to this email.</p>
                   <p>Thanks!</p>
                   <p>Foodyverse team</p>
                   <a href=\"mailto://help@foodyverse.ee\">help@foodyverse.ee</a>
                   "
    end

    def send_guest_request_email(table_guest) do
        send_email to: table_guest.joined_dining_table.host.email,
                    from: @from,
                    subject: "FoodyVerse Guest Request",
                    # attachments: [%{path: "salemove.pdf" , filename: "sale.pdf"}],
                    html: " <strong>Hello!</strong>
                    <p> #{table_guest.guest.first_name} #{table_guest.guest.last_name} has requested to join your event</p>
                    <p>To review your guest's request, click the link below.</p>
                    
                   <div>
                       <a href=\"https://foodyverse.ee/#/table-view/#{table_guest.dining_table_id}\">Review Event</a> 
                   </div>

                                      
                   <p>Thanks!</p>
                   <p>Foodyverse team</p>
                   <a href=\"mailto://help@foodyverse.ee\">help@foodyverse.ee</a>
                   "
    end

    def send_guest_accepted_email(table_guest, host) do
        send_email to: table_guest.guest.email,
                    from: @from,
                    subject: "FoodyVerse Guest Request Accepted",
                    # attachments: [%{path: "salemove.pdf" , filename: "sale.pdf"}],
                    html: " <strong>Hello!</strong>
                    <p>Your request to join #{host.first_name} #{host.last_name} on #{table_guest.joined_dining_table.title} 
                    at #{table_guest.joined_dining_table.location} has been accepted!</p>
                   
                    <p>Follow the link below to view</p>
                    <div>
                       <a href=\"https://foodyverse.ee/#/table-view/#{table_guest.dining_table_id}\">Click here</a> 
                   </div>
                    <p>Thanks!</p>
                    <p>Foodyverse team</p>
                    <a href=\"mailto://help@foodyverse.ee\">help@foodyverse.ee</a>
                   "
    end

    def send_guest_declined_email(table_guest, host) do
        send_email to: table_guest.guest.email,
                    from: @from,
                    subject: "FoodyVerse Guest Request Declined",
                    # attachments: [%{path: "salemove.pdf" , filename: "sale.pdf"}],
                    html: " <strong>Hello!</strong>
                    <p>Oops, you cannot join #{host.first_name} #{host.last_name} on #{table_guest.joined_dining_table.title}
                        at this moment. Please look for another event to join on our platform.</p>
                   <p>Thanks!</p>
                   <p>Foodyverse team</p>
                   <a href=\"mailto://help@foodyverse.ee\">help@foodyverse.ee</a>
                   "
    end

    def send_accepted_guest_cancelled_email(table_guest, host) do
        send_email to: host.email,
                    from: @from,
                    subject: "FoodyVerse Meal Cancellation",
                    # attachments: [%{path: "salemove.pdf" , filename: "sale.pdf"}],
                    html: " <strong>Hello!</strong>
                    <p>Something came up! #{table_guest.guest.first_name} #{table_guest.guest.last_name} 
                        can no longer make it to #{table_guest.joined_dining_table.title} </p>
                   <p>Thanks!</p>
                   <p>Foodyverse team</p>
                   <a href=\"mailto://help@foodyverse.ee\">help@foodyverse.ee</a>
                   "
    end

    def send_change_password_link_email(user) do
        send_email to: user.email,
                    from: @from,
                    subject: "Reset your Foodyverse password",
                    # attachments: [%{path: "salemove.pdf" , filename: "sale.pdf"}],
                    html: " <strong>Hi #{user.first_name} #{user.last_name},</strong>
                    <p>Someone recently requested a password change for your Foodyverse account. If this was you, you can set a new password here:
                    <div>
                       <a href=\"https://foodyverse.ee/#/change-password/#{user.id}\">Reset Password</a> 
                   </div>
                   <p>If you don't want to change your password or didn't request this, just ignore and delete this message.</p>
                   
                   <p>To keep your account secure, please don't forward this email to anyone.</p>

                   <p>Thanks!</p>
                   <p>Foodyverse team</p>
                   <a href=\"mailto://help@foodyverse.menu\">help@foodyverse.menu</a>
                   "
    end
end
  