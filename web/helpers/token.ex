defmodule Chefskitchen.Token do
    @moduledoc """
    Handles creating and validating tokens.
    """
  
    @account_activation_salt "account verification salt"
  
    def generate_activation_token(%Chefskitchen.User{id: user_id}) do
      Phoenix.Token.sign(Chefskitchen.Endpoint, @account_activation_salt, user_id)
    end

    def verify_activation_token(token) do
        max_age = 86_400 # tokens that are older than a day should be invalid
        Phoenix.Token.verify(Chefskitchen.Endpoint, @account_activation_salt, token, max_age: max_age)
    end
  
end