defmodule Chefskitchen.Utils do

    def process_image(params, pic_key) do
        params = 
            Enum.map(params, fn({key, value}) -> {String.to_atom(key), value} end) 
            |> Enum.into(%{})
        
        case is_nil(params[String.to_atom(pic_key)]) do
            true -> params
            false -> case params[String.to_atom(pic_key)] =~ "https" do
                true -> {_, params} = Map.pop(params, String.to_atom(pic_key))
                        params
                false -> {:ok, image_binary} =  Base.decode64(params[String.to_atom(pic_key)])
                        filename = 
                            image_binary
                            |> image_extension()
                            |> unique_filename()
                        File.write("uploads/#{filename}", image_binary)
                        image = 
                            %Plug.Upload{content_type: "image/#{image_extension(image_binary)}", 
                                        filename: filename, path: "uploads/#{filename}"}
                        Map.put(params, String.to_atom(pic_key), image)
            end
                     
        end
    end
    
    defp unique_filename(extension) do
		"#{UUID.uuid4(:hex)}.#{extension}"
	end
    
    defp image_extension(<<0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A, _::binary>>), do: "png"
  	defp image_extension(<<0xff, 0xD8, _::binary>>), do: "jpg"

end