defimpl Canada.Can, for: Atom do
    # When the user is not logged in, all they can do is read Posts
    def can?(nil, :full_index, %Chefskitchen.DiningTable{}), do: true
    def can?(nil, _, _), do: false
end

defimpl Canada.Can, for: Chefskitchen.User do
    # def can?(user, action, Chefskitchen.DiningTable) when action in [:index, :show, :full_index] do
    #     user.role == "foody" or user.role == "cooky"
    # end
    
    def can?(user, action, Chefskitchen.User) when action in [:index] do
        Enum.member?(user.roles, "ADMIN")
    end
    
    def can?(user, action, Chefskitchen.DiningTable) when action in [:create] do
        Enum.member?(user.roles, "HOST")
    end

    
end