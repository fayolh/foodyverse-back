defmodule Chefskitchen.UserApiController do
	use Chefskitchen.Web, :controller
	import Canary.Plugs
	alias Chefskitchen.{User, DiningTable}

	plug :authorize_resource, model: User, only: [:index]

	def index(conn, _params) do
		users = Repo.all(User) 
		|> Enum.map(fn user -> %{ id: user.id, email: user.email, phone: user.phone, first_name: user.first_name, 
								last_name: user.last_name, languages: user.languages, about: user.about, 
								address: user.address, roles: user.roles, gender: user.gender, status: user.status,
								profile_picture: User.image_url(user)} end)
		
		conn
    	|> put_status(200)
    	|> json(users)
  	end

	def create(conn, user_params) do
		params = Chefskitchen.Utils.process_image(user_params, "profile_picture")
		changeset = User.changeset(%User{}, params)
		IO.inspect changeset
    	case Repo.insert(changeset) do
			{:ok, user} ->
				case Map.has_key?(params, :social) do
					true -> mark_as_active(conn, user)
					false -> 
						token = Chefskitchen.Token.generate_activation_token(user)
						activation_url = "https://foodyverse.ee/#/activation?token=#{token}"
						# "http://localhost:8080/#/activation?token=#{token}"
						# user_api_url(conn, :activate, token: token)
						Chefskitchen.Mailer.send_activation_email(user, activation_url)
						conn
						|> put_status(201)
						|> json(%{msg: "User successfully created"}) #Created
			   	end
          	{:error, changeset} ->
            	conn
            	|> put_status(400)
				|> json(Ecto.Changeset.traverse_errors(changeset, fn {msg, opts} -> 
						Enum.reduce(opts, msg, fn {key, value}, acc -> 
							String.replace(acc, "%{#{key}}", to_string(value))end) end))
    	end
	end

	def activate(conn, %{"token" => token}) do
		{:ok, user_id} = Chefskitchen.Token.verify_activation_token(token)
		user = Repo.get(User, user_id) 
		if user.verified == false do
			mark_as_active(conn, user)
		else
			conn
			|> put_status(400)
			|> json(%{msg: "inexistant account or already activated"})
		end
	end
	
	def activate(conn, _) do
		# If there is no token in our params, tell the user they've provided
		# an invalid token or expired token
		conn
		|> put_status(400)
		|> json(%{msg: "invalid verification link"})
	end

	def update(conn, user_params) do
		params = Chefskitchen.Utils.process_image(user_params, "profile_picture")
		user = Repo.get!(User, params.id)
		changeset = User.update_changeset(user, params)
		update_user(conn, changeset, "User successfully updated")
  	end

  	def show(conn, %{"id" => id}) do
    	user = Repo.get!(User, id)
    	conn
    	|> put_status(200)
    	|> json(%{ id: user.id, email: user.email, phone: user.phone, first_name: user.first_name, 
					last_name: user.last_name, languages: user.languages, about: user.about, 
					address: user.address, roles: user.roles, gender: user.gender, status: user.status, created_at:  user.inserted_at,
					profile_picture: User.image_url(user)})
  	end
	
	def tables(conn, %{"id" => id}) do
		user = Repo.get!(User, id) |> Repo.preload([:joined_dining_tables, :hosted_dining_tables])
		joined_tables = Enum.map(user.joined_dining_tables, 
						fn table -> Repo.get!(DiningTable, table.dining_table_id)|> Repo.preload(:host) end)
						|> Enum.map(fn table -> %{table_id: table.id, title: table.title, status: table.status,
						location: table.location, description: table.description, fee: table.fee, 
						no_of_guests: table.number_of_guests, ingredients: table.ingredients, time: table.time,
						host: %{id: table.host.id, name: "#{table.host.first_name} #{table.host.last_name}"},
						image: DiningTable.image_url(table), payment_type: table.payment_type}end)
		hosted_tables = Enum.map(user.hosted_dining_tables, 
						fn table -> Repo.get!(DiningTable, table.id)|> Repo.preload(:host) end)
						|> Enum.map(fn table -> %{table_id: table.id, title: table.title, status: table.status,
						location: table.location, description: table.description, fee: table.fee, 
						no_of_guests: table.number_of_guests, ingredients: table.ingredients, time: table.time,
						host: %{id: table.host.id, name: "#{table.host.first_name} #{table.host.last_name}"},
						image: DiningTable.image_url(table), payment_type: table.payment_type}end)
		conn
    	|> put_status(200)
    	|> json(%{joined_tables: joined_tables, hosted_tables: hosted_tables})
	end
	
  	def delete(conn, %{"id" => id}) do
		user = Repo.get!(User, id) 
		mark_as_inactive(conn, user)
    	# Repo.delete!(user)

    	# conn
    	# |> put_status(200) #Need to check
    	# |> json(%{msg: "User successfully deleted"})
  	end

  	def current(conn, _params) do
    	user = Guardian.Plug.current_resource(conn) 
    	conn
    	|> put_status(200)
    	|> json(%{ id: user.id, email: user.email, phone: user.phone, first_name: user.first_name, 
					last_name: user.last_name, languages: user.languages, about: user.about, 
					address: user.address, roles: user.roles, gender: user.gender, created_at:  user.inserted_at})
  	end

  	def change_password(conn, user_params) do
    	user = Repo.get!(User, user_params["id"])
    	changeset = User.changeset(user, %{password: user_params["password"], password_confirmation: user_params["password_confirmation"]})
    	update_user(conn, changeset, "Password successfully changed")
	end

	def password_change(conn, %{"email" => email}) do
		user = Repo.get_by(User, email: email)
		if(user)do
			Chefskitchen.Mailer.send_change_password_link_email(user)
			conn
			|> put_status(200)
			|> json(%{msg: "An email was sent to your address with instructions!"})
		else
			conn
			|> put_status(500)
			|> json(%{msg: "Email not registered in the system"})
		end
	end

	def resend_email(conn, %{"email" => email}) do
		user = Repo.get_by(User, email: email)
		if(user)do
			token = Chefskitchen.Token.generate_activation_token(user)
			activation_url = "https://foodyverse.ee/#/activation?token=#{token}"
			# user_api_url(conn, :activate, token: token)
    		Chefskitchen.Mailer.send_activation_email(user, activation_url)
			conn
			|> put_status(200)
			|> json(%{msg: "Email sent successfully!"})
		else
			conn
			|> put_status(500)
			|> json(%{msg: "Email not registered in the system"})
		end
	end

	defp update_user(conn, changeset, msg) do
    	case Repo.update(changeset) do
			  {:ok, _user} ->
				conn
				|> put_status(201)
				|> json(%{msg: msg})
			{:error, _changeset} ->
				conn
				|> put_status(500)
				|> json(%{msg: "Backend Error"})
    	end
	end
	
	defp mark_as_active(conn, user) do
		changeset = User.activation_changeset(user, %{verified: !user.verified, status: "ACTIVE"})
    	update_user(conn, changeset, "account activated")
	end

	defp mark_as_inactive(conn, user) do
		changeset = User.activation_changeset(user, %{status: "INACTIVE"})
    	update_user(conn, changeset, "account deleted")
	end

end
