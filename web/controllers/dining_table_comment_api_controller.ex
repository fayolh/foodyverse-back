defmodule Chefskitchen.DiningTableCommentApiController do
    use Chefskitchen.Web, :controller
    alias Chefskitchen.{Repo,User,DiningTable,DiningTableComment}

    def create(conn, dining_table_comment_params) do
        table = Repo.get(DiningTable, dining_table_comment_params["table_id"])
        user = Repo.get(User, Guardian.Plug.current_resource(conn).id)
        if table do
            if user do
                changeset = DiningTableComment.changeset(%DiningTableComment{}, %{content: dining_table_comment_params["content"]})
                            |> Ecto.Changeset.put_assoc(:user, user)
                            |> Ecto.Changeset.put_assoc(:dining_table, table)

    	        case Repo.insert(changeset) do
        	        {:ok, _dining_table_comment} ->
            	        conn
            	        |> put_status(201)
            	        |> json(%{msg: "Comment successfully added"}) #Created
          	        {:error, _changeset} ->
            	        conn
            	        |> put_status(500)
            	        |> json(%{msg: "Backend Error"})
    	        end
            else
                conn
                |> put_status(403)
                |> json(%{msg: "User not found"}) #Error
            end
        else
            conn
            |> put_status(403)
            |> json(%{msg: "Table not found"}) #Error
        end
        
    end
      
      def index(conn,  %{"table_id" => table_id}) do
        table = Repo.get(DiningTable, table_id)
        if table do
            query = from dt in DiningTableComment,
            where: dt.dining_table_id == ^table_id,
			order_by:  dt.inserted_at,
            select: dt
		    comments = Repo.all(query) |> Repo.preload([:user]) #|> Repo.preload(:ingredients)
                |> Enum.map(fn comment -> 
                     getComment(comment)
                end) 
		
		    conn
    	    |> put_status(200)
            |> json(comments)
        else
            conn
            |> put_status(403)
            |> json(%{msg: "Table not found"}) #Error
        end
      end
            
      def delete(conn,  %{"id" => id}) do
        table_comment = Repo.get!(DiningTableComment,id)
        if table_comment do
            Repo.delete!(table_comment)
            conn
            |> put_status(200)
            |> json(%{msg: "Comment successfully deleted"})
        else
            conn
            |> put_status(403)
            |> json(%{msg: "Comment not found"}) #Error
        end
      end
      
      def show(conn,  %{"id" => id}) do
        table_comment = Repo.get(DiningTableComment,id) |> Repo.preload([:user])
        if table_comment do
            conn
            |> put_status(200)
            |> json(getComment(table_comment))
        else
            conn
            |> put_status(403)
            |> json(%{msg: "Comment not found"}) #Error
        end
      end
      
      defp getComment(comment) do
        %{ id: comment.id, time: comment.inserted_at ,user:  %{user_id: comment.user.id, fullname: comment.user.first_name <> " " <> comment.user.last_name, profile_picture: User.image_url(comment.user)}, content: comment.content}
      end

      
     
end