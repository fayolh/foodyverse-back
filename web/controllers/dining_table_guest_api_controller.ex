defmodule Chefskitchen.DiningTableGuestApiController do
    use Chefskitchen.Web, :controller
    alias Chefskitchen.{Repo,User,DiningTable,DiningTableGuest}

    def create(conn, dining_table_guest_params) do
        table = Repo.get(DiningTable, dining_table_guest_params["table_id"]) |> Repo.preload(:host)
        guest = Repo.get(User, Guardian.Plug.current_resource(conn).id) #conn.assigns.current_user.id
        if table do
            if guest do
                changeset = DiningTableGuest.changeset(%DiningTableGuest{}, %{})
                            |> Ecto.Changeset.put_assoc(:guest, guest)
                            |> Ecto.Changeset.put_assoc(:joined_dining_table, table)

    	        case Repo.insert(changeset) do
                    {:ok, dining_table_guest} ->
                        Chefskitchen.Mailer.send_guest_request_email(dining_table_guest)
            	        conn
            	        |> put_status(201)
            	        |> json(%{msg: "Guest request successful"}) #Created
          	        {:error, changeset} ->
            	        conn
            	        |> put_status(400)
            	        |> json(Ecto.Changeset.traverse_errors(changeset, fn {msg, opts} -> 
                            Enum.reduce(opts, msg, fn {key, value}, acc -> 
                                String.replace(acc, "%{#{key}}", to_string(value))end) end))
    	        end
            else
                conn
                |> put_status(403)
                |> json(%{msg: "Guest not found"}) #Error
            end
        else
            conn
            |> put_status(403)
            |> json(%{msg: "Table not found"}) #Error
        end
        
    end
      
    def index(conn,  %{"table_id" => table_id}) do
        query = from dt in DiningTableGuest,
            where: dt.dining_table_id == ^table_id,
            select: dt
        guests = Repo.all(query) |> Repo.preload([:guest])
                |> Enum.map(fn guest -> 
                    %{ guest_id: guest.id, user: getUser(guest.guest), status: guest.status} end) # 
        
        conn
        |> put_status(200)
        |> json(guests)
    end
      
    def update(conn,  %{"id" => id, "status"=> status}) do
        table_guest = Repo.get(DiningTableGuest,id) |> Repo.preload([:guest, :joined_dining_table])
        if table_guest do
            changeset = DiningTableGuest.changeset(table_guest,%{status: status})
            case Repo.update(changeset) do
                {:ok, table_guest} ->
                    host = Repo.get(User, table_guest.joined_dining_table.user_id)
                    case status do
                        "ACCEPTED" -> Chefskitchen.Mailer.send_guest_accepted_email(table_guest, host)
                        "REJECTED" -> Chefskitchen.Mailer.send_guest_declined_email(table_guest, host)  
                        "CANCELLED" -> Chefskitchen.Mailer.send_accepted_guest_cancelled_email(table_guest, host)    
                    end
                    conn
                    |> put_status(200)
                    |> json(%{msg: "Guest status successfully changed"}) #Created
                {:error, changeset} ->
                    conn
                    |> put_status(500)
                    |> json(Ecto.Changeset.traverse_errors(changeset, fn {msg, opts} -> 
                        Enum.reduce(opts, msg, fn {key, value}, acc -> 
                            String.replace(acc, "%{#{key}}", to_string(value))end) end))
            end
        else
            conn
            |> put_status(403)
            |> json(%{msg: "Guest not found"}) #Error
        end
    end
      
    def delete(conn,  %{"id" => id}) do
        table_guest = Repo.get!(DiningTableGuest,id)
        if table_guest do
            Repo.delete!(table_guest)
            conn
            |> put_status(200)
            |> json(%{msg: "Guest request successfully cancelled"}) #Created
        else
            conn
            |> put_status(403)
            |> json(%{msg: "Guest not found"}) #Error
        end
    end
      
    def show(conn,  %{"id" => id}) do
        table_guest = Repo.get(DiningTableGuest,id)
        if table_guest do
            conn
            |> put_status(200)
            |> json(%{ guest_id: table_guest.id, user_id: table_guest.user_id, status: table_guest.status}) #Created
        else
            conn
            |> put_status(403)
            |> json(%{msg: "Guest not found"}) #Error
        end
    end 

    defp getUser(user) do
        %{ id: user.id, email: user.email, phone: user.phone, first_name: user.first_name, 
					last_name: user.last_name, languages: user.languages, about: user.about, 
					address: user.address, roles: user.roles, gender: user.gender, status: user.status, created_at:  user.inserted_at,
					profile_picture: User.image_url(user)}
    end

end