defmodule Chefskitchen.PageController do
  use Chefskitchen.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
