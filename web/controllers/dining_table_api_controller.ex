defmodule Chefskitchen.DiningTableApiController do
    use Chefskitchen.Web, :controller
    import Canary.Plugs
    alias Chefskitchen.{Repo,User,DiningTable}
    plug :authorize_resource, model: DiningTable, only: [:create]

    def full_index(conn, params) do
        query = from dt in DiningTable, select: dt
        query = case is_nil(params["title"]) do 
            true -> query 
            false -> from q in query, where: ilike(q.title, ^"%#{params["title"]}%")
        end
        query = case is_nil(params["start"]) do
            true -> query
            false -> start_date = Ecto.DateTime.cast!(params["start"] <> " 00:00:00") |> Ecto.DateTime.to_erl |> NaiveDateTime.from_erl!
                 from q in query, where: q.time >= ^start_date     
        end
        query = case is_nil(params["end"]) do
            true -> query
            false -> end_date = Ecto.DateTime.cast!(params["end"] <> " 23:59:00") |> Ecto.DateTime.to_erl |> NaiveDateTime.from_erl!
                 from q in query, where: q.time <= ^end_date     
        end

		dining_tables = Repo.all(query) # |> Repo.preload(:ingredients)
        |> Enum.map(fn dining_table -> %{ table_id: dining_table.id, host_id: dining_table.user_id, title: dining_table.title,
                        description: dining_table.description, no_of_guests: dining_table.number_of_guests, status: dining_table.status,
                        ingredients: dining_table.ingredients, location: dining_table.location, time: dining_table.time |> Timex.format!("%Y-%m-%d %H:%M", :strftime), fee: dining_table.fee,
                        image: DiningTable.image_url(dining_table) } end)
		
		conn
    	|> put_status(200)
    	|> json(dining_tables)
    end
      
    def index(conn, %{"host_id" => host_id}) do
        query = from dt in DiningTable,
            where: dt.user_id == ^host_id,
            select: dt
		dining_tables = Repo.all(query)
        |> Enum.map(fn dining_table -> %{ table_id: dining_table.id, user_id: dining_table.user_id, title: dining_table.title,
                        description: dining_table.description, no_of_guests: dining_table.number_of_guests, status: dining_table.status,
                        ingredients: dining_table.ingredients, location: dining_table.location, time: dining_table.time, fee: dining_table.fee, 
                        image: DiningTable.image_url(dining_table), payment_type: dining_table.payment_type} end)
		
		conn
    	|> put_status(200)
    	|> json(dining_tables)
  	end

    def create(conn, dining_table_params) do
        dining_table_params = Chefskitchen.Utils.process_image(dining_table_params, "image")
        # dining_table_params 
        #     |> Enum.map(fn({key, value}) -> {String.to_atom(key), value} end)
        #     |> Enum.into(%{})
        changeset = DiningTable.changeset(%DiningTable{}, 
            %{ dining_table_params | time: Timex.parse!(dining_table_params.time, "%Y-%m-%d %H:%M:%S", :strftime)})
            |> Ecto.Changeset.put_assoc(:host, Repo.get!(User, dining_table_params.host_id))
    	case Repo.insert(changeset) do
        	{:ok, dining_table} ->
            	conn
            	|> put_status(201)
            	|> json(%{msg: "Dining table successfully created", id: dining_table.id}) #Created
          	{:error, _changeset} ->
            	conn
            	|> put_status(500)
            	|> json(%{msg: "Backend Error"})
    	end
  	end

  	def show(conn, %{"id" => id}) do
    	dining_table = Repo.get!(DiningTable, id)
    	conn
    	|> put_status(200)
        |> json(%{ table_id: dining_table.id, user_id: dining_table.user_id, title: dining_table.title, description: dining_table.description, 
                    number_of_guests: dining_table.number_of_guests, status: dining_table.status, ingredients: dining_table.ingredients,
                    location: dining_table.location, time: dining_table.time, fee: dining_table.fee, 
                    payment_type: dining_table.payment_type, image: DiningTable.image_url(dining_table) })
    end

    def update(conn, params) do
        params = Chefskitchen.Utils.process_image(params, "image")
        table = Repo.get!(DiningTable, params.id)
        changeset = DiningTable.changeset(table, params)
        case Repo.update(changeset) do
            {:ok, _table} ->
              conn
              |> put_status(200)
              |> json(%{msg: "updated succesful"})
          {:error, _changeset} ->
              conn
              |> put_status(500)
              |> json(%{msg: "Backend Error"})
        end
    end 
    
    
    def update_ingredients(conn, params) do
        table = Repo.get!(DiningTable, params["table_id"])
        changeset = DiningTable.changeset(table, %{ingredients: params["ingredients"]})
        case Repo.update(changeset) do
            {:ok, _table} ->
              conn
              |> put_status(200)
              |> json(%{msg: "ingredient list updated succesfully"})
          {:error, _changeset} ->
              conn
              |> put_status(500)
              |> json(%{msg: "Backend Error"})
        end
    end   
end