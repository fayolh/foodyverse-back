defmodule Chefskitchen.DiningTableChatApiController do
    use Chefskitchen.Web, :controller
    alias Chefskitchen.{Repo,User,DiningTable,DiningTableChat}

    def create(conn, dining_table_chat_params) do
        table = Repo.get(DiningTable, dining_table_chat_params["table_id"])
        user = Repo.get(User, Guardian.Plug.current_resource(conn).id)
        if table do
            if user do
                changeset = DiningTableChat.changeset(%DiningTableChat{}, %{content: dining_table_chat_params["content"]})
                            |> Ecto.Changeset.put_assoc(:user, user)
                            |> Ecto.Changeset.put_assoc(:dining_table, table)

    	        case Repo.insert(changeset) do
        	        {:ok, _dining_table_chat} ->
            	        conn
            	        |> put_status(201)
            	        |> json(%{msg: "Chat message successfully added"}) #Created
          	        {:error, _changeset} ->
            	        conn
            	        |> put_status(500)
            	        |> json(%{msg: "Backend Error"})
    	        end
            else
                conn
                |> put_status(403)
                |> json(%{msg: "User not found"}) #Error
            end
        else
            conn
            |> put_status(403)
            |> json(%{msg: "Table not found"}) #Error
        end
        
    end
      
      def index(conn,  %{"table_id" => table_id}) do
        table = Repo.get(DiningTable, table_id)
        if table do
            query = from dt in DiningTableChat,
            where: dt.dining_table_id == ^table_id,
			order_by:  dt.inserted_at,
            select: dt
		    chats = Repo.all(query) |> Repo.preload([:user]) #|> Repo.preload(:ingredients)
                |> Enum.map(fn chat -> 
                     getChat(chat)
                end) 
		
		    conn
    	    |> put_status(200)
            |> json(chats)
        else
            conn
            |> put_status(403)
            |> json(%{msg: "Table not found"}) #Error
        end
      end
            
      def delete(conn,  %{"id" => id}) do
        table_chat = Repo.get!(DiningTableChat, id)
        if table_chat do
            Repo.delete!(table_chat)
            conn
            |> put_status(200)
            |> json(%{msg: "Chat message successfully deleted"})
        else
            conn
            |> put_status(403)
            |> json(%{msg: "Chat message not found"}) #Error
        end
      end
      
      def show(conn,  %{"id" => id}) do
        table_chat = Repo.get(DiningTableChat,id) |> Repo.preload([:user])
        if table_chat do
            conn
            |> put_status(200)
            |> json(getChat(table_chat))
        else
            conn
            |> put_status(403)
            |> json(%{msg: "Chat message not found"}) #Error
        end
      end
      
      defp getChat(chat) do
        %{ id: chat.id, time: chat.inserted_at ,user:  %{user_id: chat.user.id, fullname: chat.user.first_name <> " " <> chat.user.last_name, profile_picture: User.image_url(chat.user)}, content: chat.content}
      end

      
     
end