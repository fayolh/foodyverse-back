defmodule Chefskitchen.SessionApiController do
  	use Chefskitchen.Web, :controller
  	alias Chefskitchen.{Repo,User,Authentication}

	def create(conn, %{"email" => email, "password" => password}) do
		user = Repo.get_by(User, email: email)  
		case is_nil(user) do
			true -> conn
					|> put_status(400)
					|> json(%{message: "Invalid account"})
			false ->
				if (user.verified) do
					case Authentication.check_credentials(user, password) do
						{:ok, _conn} ->
							{:ok, jwt, _full_claims} = Guardian.encode_and_sign(user, :token)
							conn
							|> put_status(201)
							|> json(%{token: jwt, id: user.id, roles: user.roles})
						
						{:error, _conn} ->
							conn
							|> put_status(400)
							|> json(%{message: "Bad credentials"})
					end
				else
					conn 
					|> put_status(400)
					|> json(%{message: "User not activated!"})
				end
		end
  	end

  	def delete(conn, _params) do
		{:ok, claims} = Guardian.Plug.claims(conn)
    	conn
    	|> Guardian.Plug.current_token
    	|> Guardian.revoke!(claims)

    	conn
    	|> json(%{msg: "Good bye"})
	end
	def unauthenticated(conn, _params) do
		conn
		|> put_status(401)
		|> json(%{msg: "Unauthenticated"}) #Unauthorized
		|> halt
	end

	def unauthorized(conn) do
		conn
		|> put_status(403)
		|> json(%{msg: "Unauthorized"})
		|> halt
	end
end
