defmodule Chefskitchen.Avatar do
  use Arc.Definition
  use Arc.Ecto.Definition
  
  @versions [:original]
  @extension_whitelist ~w(.jpg .jpeg .png)
  @acl :public_read
  
  # def acl(:thumb, _), do: :public_read

  def validate({file, _}) do 
    file_extension = file.file_name |> Path.extname |> String.downcase
    Enum.member?(@extension_whitelist, file_extension)
  end

  # def transform(:thumb, _) do
  #   {:convert, "-thumbnail 150x150^ -gravity center -extent 150x150 -format png", :png}
  # end

  def storage_dir(_version, {_file, scope}) do 
    "/avatars/#{scope.id}"
  end

  # def filename(_version, {_file, _scope}) do
  #   UUID.uuid4(:hex)
  # end

  # Provide a default URL if there hasn't been a file uploaded
  def default_url(:original) do 
    "https://s3-eu-west-1.amazonaws.com/foodyverse/avatar-default.png"
  end

  def s3_object_headers(_version, {file, _scope}) do
    # For "image.png", would produce: "image/png" 
    [content_type: MIME.from_path(file.file_name)]
  end

end
