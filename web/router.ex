defmodule Chefskitchen.Router do
  use Chefskitchen.Web, :router

  def guardian_current_user(conn, _) do
    Plug.Conn.assign(conn, :current_user, Guardian.Plug.current_resource(conn))
  end

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  scope "/", Chefskitchen do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug Guardian.Plug.VerifyHeader
    plug Guardian.Plug.LoadResource
  end

  pipeline :auth_api do
    plug Guardian.Plug.VerifySession
    plug Guardian.Plug.EnsureAuthenticated, handler: Chefskitchen.SessionApiController  
    plug :guardian_current_user
  end

  scope "/api", Chefskitchen do
    pipe_through :api
    post "/users", UserApiController, :create
    get "/users/activate", UserApiController, :activate
    patch "/users/:id/change-password", UserApiController, :change_password
    get "/users/password-change", UserApiController, :password_change
    get "/users/resend-email", UserApiController, :resend_email
    post "/sessions", SessionApiController, :create
    get "/tables", DiningTableApiController, :full_index
  end

  scope "/api", Chefskitchen do
    pipe_through [:api, :auth_api] 
    delete "/sessions/:id", SessionApiController, :delete
    resources "/users", UserApiController, only: [:update, :show, :delete, :index]
    get "/users/:id/tables", UserApiController, :tables
    # patch "/users/:id/change-password", UserAPIController, :change_password
    # get "/users/current", UserApiController, :current
    resources "/hosts", UserApiController, only: [], name: :host, param: :id  do
      resources "/tables", DiningTableApiController, only: [:create, :show, :update, :delete, :index], name: :table do
        resources "/guests", DiningTableGuestApiController, only: [:create, :index, :update, :delete, :show]
        resources "/comments", DiningTableCommentApiController, only: [:create, :index, :update, :delete, :show]
        resources "/chat", DiningTableChatApiController, only: [:create, :index, :update, :delete, :show]
        put "/ingredients", DiningTableApiController, :update_ingredients
      end
    end
  end
end
