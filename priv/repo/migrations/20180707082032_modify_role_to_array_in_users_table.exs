defmodule Chefskitchen.Repo.Migrations.ModifyRoleToArrayInUsersTable do
  use Ecto.Migration

  def change do
    alter table(:users) do 
      remove :role
      add :roles, {:array, :string}, default: fragment("ARRAY['HOST']")
    end
  end
end
