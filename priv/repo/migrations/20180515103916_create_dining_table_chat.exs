defmodule Chefskitchen.Repo.Migrations.CreateDiningTableChat do
  use Ecto.Migration

  def change do
    create table(:dining_table_chats) do
      add :content, :string
      add :user_id, references(:users, on_delete: :nothing)
      add :dining_table_id, references(:dining_tables, on_delete: :nothing)

      timestamps()
    end
    # create index(:dining_table_comments, [:user_id])
    # create index(:dining_table_comments, [:dining_table_id])
    create index(:dining_table_comments, [:user_id, :dining_table_id], name: :dining_table_chats_user_id_dining_table_id_index)

  end
end
