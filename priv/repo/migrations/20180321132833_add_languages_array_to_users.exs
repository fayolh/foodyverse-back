defmodule Chefskitchen.Repo.Migrations.AddLanguagesArrayToUsers do
  use Ecto.Migration

  def change do
    alter table(:users) do 
      add :languages, {:array, :string}
    end
  end
end
