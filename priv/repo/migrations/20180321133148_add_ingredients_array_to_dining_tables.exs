defmodule Chefskitchen.Repo.Migrations.AddIngredientsArrayToDiningTables do
  use Ecto.Migration

  def change do
    alter table(:dining_tables) do 
      add :ingredients, {:array, :string}
    end
  end
end
