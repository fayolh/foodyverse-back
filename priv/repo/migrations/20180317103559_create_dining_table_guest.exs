defmodule Chefskitchen.Repo.Migrations.CreateDiningTableGuest do
  use Ecto.Migration

  def change do
    create table(:dining_table_guests) do
      add :status, :string
      add :user_id, references(:users, on_delete: :nothing)
      add :dining_table_id, references(:dining_tables, on_delete: :nothing)

      timestamps()
    end
    create unique_index(:dining_table_guests, [:user_id, :dining_table_id], name: :dining_table_guests_user_id_dining_table_id_index)
  end
end
