defmodule Chefskitchen.Repo.Migrations.CreateDiningTableComment do
  use Ecto.Migration

  def change do
    create table(:dining_table_comments) do
      add :content, :string
      add :user_id, references(:users, on_delete: :nothing)
      add :dining_table_id, references(:dining_tables, on_delete: :nothing)

      timestamps()
    end
    create index(:dining_table_comments, [:user_id])
    create index(:dining_table_comments, [:dining_table_id])
  end
end
