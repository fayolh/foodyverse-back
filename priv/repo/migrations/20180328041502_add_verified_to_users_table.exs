defmodule Chefskitchen.Repo.Migrations.AddVerifiedToUsersTable do
  use Ecto.Migration

  def change do
    alter table(:users) do 
      add :verified, :boolean
    end
  end
end
