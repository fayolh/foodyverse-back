defmodule Chefskitchen.Repo.Migrations.AboutToTextInUsersTable do
  use Ecto.Migration

  def change do
    alter table(:users) do
      modify :about, :text
    end
  end
end
