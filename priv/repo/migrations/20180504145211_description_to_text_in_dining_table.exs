defmodule Chefskitchen.Repo.Migrations.DescriptionToTextInDiningTable do
  use Ecto.Migration

  def change do
    alter table(:dining_tables) do
      modify :description, :text
    end
  end
end
