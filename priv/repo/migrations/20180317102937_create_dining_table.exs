defmodule Chefskitchen.Repo.Migrations.CreateDiningTable do
  use Ecto.Migration

  def change do
    create table(:dining_tables) do
      add :title, :string
      add :description, :string
      add :number_of_guests, :integer
      add :status, :string
      add :location, :string
      add :fee, :float
      add :time, :naive_datetime
      add :payment_type, :string
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end

    create index(:dining_tables, [:user_id])
  end
end
