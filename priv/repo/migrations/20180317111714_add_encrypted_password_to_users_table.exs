defmodule Chefskitchen.Repo.Migrations.AddEncryptedPasswordToUsersTable do
  use Ecto.Migration

  def change do
    alter table(:users) do 
      add :encrypted_password, :string
      remove :password
    end
  end
end
