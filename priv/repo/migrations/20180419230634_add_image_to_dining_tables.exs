defmodule Chefskitchen.Repo.Migrations.AddImageToDiningTables do
  use Ecto.Migration

  def change do
    alter table(:dining_tables) do 
      add :image, :string
    end
  end
end
