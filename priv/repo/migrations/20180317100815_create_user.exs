defmodule Chefskitchen.Repo.Migrations.CreateUser do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :last_name, :string
      add :first_name, :string
      add :email, :string
      add :phone, :string
      add :password, :string
      add :address, :string
      add :about, :string
      add :role, :string

      timestamps()
    end
    create unique_index(:users, [:email])
  end
end
