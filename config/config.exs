# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :chefskitchen,
  ecto_repos: [Chefskitchen.Repo]

# Configures the endpoint
config :chefskitchen, Chefskitchen.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "RHrdxeAnNbSxa00iUhomOHLLUD8zR73LogOwJNdwHNQ8+Vy3lFkS6Cb1fdQUPFlF",
  render_errors: [view: Chefskitchen.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Chefskitchen.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"

config :guardian, Guardian,
issuer: "Chefskitchen",
ttl: { 30, :days },
allowed_drift: 2000,
secret_key: "UeXmjTHTMWpC4CWySKWqL1ccy5sZfK2OAOAnyvOwUmPEtw85ZZMuoBTukzKVL79N",
serializer: Chefskitchen.GuardianSerializer

config :canary, unauthorized_handler: {Chefskitchen.SessionApiController, :unauthorized}

config :chefskitchen, 
  mailgun_domain: "https://api.mailgun.net/v3/mg.foodyverse.ee", 
  mailgun_key: "key-686378a40a50400285725bd5e6e20d13"

config :arc,
  storage: Arc.Storage.S3,
  bucket: "foodyverse",
  virtual_host: true

config :ex_aws,
  access_key_id: "AKIAIL55HLSUFY4VYYRA",
  secret_access_key: "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",
  s3: [
    scheme: "https://",
    host: "s3.eu-west-1.amazonaws.com",
    region: "eu-west-1"
  ]