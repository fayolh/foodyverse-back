defmodule Chefskitchen.UserTest do
  use Chefskitchen.ModelCase

  alias Chefskitchen.User

  @valid_attrs %{about: "some about", address: "some address", email: "some email", first_name: "some first_name", last_name: "some last_name", password: "some password", phone: "some phone", role: "some role"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = User.changeset(%User{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = User.changeset(%User{}, @invalid_attrs)
    refute changeset.valid?
  end
end
