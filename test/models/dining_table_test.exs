defmodule Chefskitchen.DiningTableTest do
  use Chefskitchen.ModelCase

  alias Chefskitchen.DiningTable

  @valid_attrs %{description: "some description", fee: 120.5, location: "some location", number_of_guests: "some number_of_guests", status: "some status", time: ~N[2010-04-17 14:00:00.000000], title: "some title"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = DiningTable.changeset(%DiningTable{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = DiningTable.changeset(%DiningTable{}, @invalid_attrs)
    refute changeset.valid?
  end
end
