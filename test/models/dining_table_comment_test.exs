defmodule Chefskitchen.DiningTableCommentTest do
  use Chefskitchen.ModelCase

  alias Chefskitchen.DiningTableComment

  @valid_attrs %{content: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = DiningTableComment.changeset(%DiningTableComment{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = DiningTableComment.changeset(%DiningTableComment{}, @invalid_attrs)
    refute changeset.valid?
  end
end
