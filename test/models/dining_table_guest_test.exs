defmodule Chefskitchen.DiningTableGuestTest do
  use Chefskitchen.ModelCase

  alias Chefskitchen.DiningTableGuest

  @valid_attrs %{status: "some status"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = DiningTableGuest.changeset(%DiningTableGuest{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = DiningTableGuest.changeset(%DiningTableGuest{}, @invalid_attrs)
    refute changeset.valid?
  end
end
